﻿// using = import en java
// mais au niveau namespace
using System;

// en C#, namespace = package en java
// (avec des accolades)
namespace Fr.EQL.AI110.MonApp
{
    public class Personne
    {
        // java : attribut + getter / setter
        // c# : Property par défaut OU attribut + Property custom

        // java : package
        // c# : namespace

        // java : import (package + classe)
        // c# : using (namespace)

        // c# : possible de passer des types valeurs par référence :
        //      si initialisation : out  (ex : TryParse)
        //      si mise à jour : ref

        // java : noms de méthodes commencent par minuscule (doSomething())
        // c# : noms de méthodes commencent par majuscule (DoSomething())

        // java : system.out.printlnt("...")
        // c# : Console.WriteLine("...")

        // java : Scanner.nextline()
        // c# : Console.ReadLine()


        // property par défaut :
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime DateNaissance { get; set; }

        // attribut + property custom :
        private string numeroSecu;
        public string NumeroSecu { 
            get { return numeroSecu; }
            set { 
                if (value.StartsWith("1") || 
                    value.StartsWith("2"))
                {
                    numeroSecu = value;
                }
                else
                {
                    throw new Exception("num secu incorrect");
                }
            }
        }

        // attribut :
        private int age;

        // property :
        public int Age
        {
            get { return age; }
            set 
            { 
                if (value < 0)
                {
                    throw new ArgumentException("age incorrect");
                }
                age = value; 
            }
        }


        // constructeur :
        public Personne(string nom, int age)
        {
            this.Nom = nom;
            this.Age = age;
        }

       


        // déclaration d'une méthode :
        public string DireBonjour(string langue)
        {
            string result;
           
            switch (langue)
            {
                case "fr":
                    result = "Salut";
                    break;
                case "es":
                    result = "Hola";
                    break;
                case "de":
                    result = "Hallo";
                    break;
                default:
                    result = "Hello";
                    break;
            }

            return result + " " + Nom + " age : " + this.Age; 
        }

        // commentaire
        /*
         * commentaire multiline
         */
    }
}
