using Fr.EQL.AI110.MonApp;

// 1 - Ajouter une dépendance sur le projet boite à outils
// 2 - Ajouter un using sur le namespace de la classe désirée :
using Fr.EQL.AI110.Demo.Tools;

public class Program
{
    public static void Main(string[] args)
    {
        string s = "tretEAZklqfsjflsAEAZE";

        Console.WriteLine(TextTools.Capitalize(s));



        Console.WriteLine("Ton nom ?");
        string nom = Console.ReadLine();

        Console.WriteLine("Votre âge ?");

        int age;
        bool ok = int.TryParse(Console.ReadLine(), out age);
        if (!ok) Console.WriteLine("Pas bien !");

        /*
        try
        {
            age = int.Parse(Console.ReadLine());
        }
        catch
        {
            Console.WriteLine("Gros naze !");
            //age = 0;
        }
        */


        Personne p = new Personne(nom, age);
        // Console.WriteLine = sysout
        Console.WriteLine(p.DireBonjour("fr"));

        //p.SetAge(p.GetAge() + 1);
        p.Age++;
        Console.WriteLine(p.Age);
        int x = p.Age;
        p.Age = 2;

        p.NumeroSecu = "18346236478628736483";

    }
}

