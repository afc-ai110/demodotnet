﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoCSharp
{
    internal class ValeurReference
    {
        public static void Main(string[] args)
        {
            int a = 0;
            Incrementer(ref a);
            Console.WriteLine(a);

            int y;
            Initialiser(out y);
            Console.WriteLine(y);   
        }

        public static void Incrementer(ref int b)
        {
            b++;
        }

        public static void Initialiser(out int x)
        {
            x = 42;
        }

    }
}
