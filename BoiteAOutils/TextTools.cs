﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.Demo.Tools
{
    public class TextTools
    {
        public static string Capitalize(string s)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(s.Substring(0, 1).ToUpper());
            sb.Append(s.Substring(1).ToLower());

            return sb.ToString();
        }

    }
}
